﻿using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Robot.Common;

namespace Levchyshyn.Olena.RobotChallange.Test
{
    [TestClass]
    public class RobotCommandsTest
    {
        [TestMethod]
        public void MoveCommandTest()
        {
            var algorithm = new LevchyshynAlgorithm();
            var map = new Map();
            var stationPos = new Position(1, 1);
            map.Stations.Add(new EnergyStation()
            {
                Energy = 1000,
                Position = stationPos,
                RecoveryRate = 2
            });
            var robots = new List<Robot.Common.Robot>()
            { new Robot.Common.Robot
                {
                    Energy = 200,
                    Position = new Position(2, 3)
                } };
            var command = algorithm.DoStep(robots, 0, map);
            Assert.IsTrue(command is MoveCommand);
            Assert.AreEqual(((MoveCommand)command).NewPosition, stationPos);
        }

        [TestMethod]
        public void CollectCommandTest()
        {
            var algorithm = new LevchyshynAlgorithm();
            var map = new Map();
            var stationPos = new Position(1, 1);
            map.Stations.Add(new EnergyStation()
            {
                Energy = 1000,
                Position = stationPos,
                RecoveryRate = 2
            });
            var robots = new List<Robot.Common.Robot>()
            { new Robot.Common.Robot
                {
                    Energy = 200,
                    Position = new Position(1, 1)
                } };
            var command = algorithm.DoStep(robots, 0, map);
            Assert.IsTrue(command is CollectEnergyCommand);
        }

        [TestMethod]
        public void CreateCommandTest()
        {
            var algorithm = new LevchyshynAlgorithm();
            var map = new Map();
        
            var robots = new List<Robot.Common.Robot>()
            { new Robot.Common.Robot
                {
                    Energy = 450,
                    Position = new Position(2, 3)
                } };
            var command = algorithm.DoStep(robots, 0, map);
            Assert.IsTrue(command is CreateNewRobotCommand);
        }      

        [TestMethod]
        public void RobotLimitOfCreationTest()
        {
            var stationPosition = new Position(1, 3);
            var robotPosition = new Position(0, 0);
            var robotToMoveIndex = 0;
            var owner = new Owner
            {
                Name = "Levchyshyn Olena",
            };

            var robots = new List<Robot.Common.Robot>();

            for (int i = 0; i < 100; ++i)
            {
                robots.Add(new Robot.Common.Robot()
                {
                    Energy = 100,
                    OwnerName = owner.Name,
                    Position = new Position(robotPosition.X + i, robotPosition.Y)
                });
            }

            var map = new Map();
            map.Stations.Add(new EnergyStation() { Energy = 100, Position = stationPosition });
            var algorithm = new LevchyshynAlgorithm();
            var command = algorithm.DoStep(robots, robotToMoveIndex, map);

            Assert.AreNotEqual(new CreateNewRobotCommand().ToString(), command.ToString());
        }
    }
}
