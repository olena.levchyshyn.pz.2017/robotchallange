﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Robot.Common;

namespace Levchyshyn.Olena.RobotChallange.Test
{
    [TestClass]
    public class DistanceHelperTest
    {
        [TestMethod]        public void getDistanceTest()        {            Position posFrom = new Position(10, 10);            Position posTo = new Position(10, 15);            Assert.AreEqual(25, DistanceHelper.getDistance(posFrom, posTo));        }
        [TestMethod]
        public void getStationTest()
        {
            Robot.Common.Robot robot = new Robot.Common.Robot()            {                Energy = 100,                Position = new Position(1, 1),                OwnerName = "Levchyshyn Olena"            };            Position nearestStationPos = new Position(1, 10);            Position middleStationPos = new Position(10, 10);            Position fatherestStationPos = new Position(10, 15);            Assert.AreEqual(nearestStationPos, DistanceHelper.getStation(nearestStationPos, robot));            Assert.AreNotEqual(middleStationPos, DistanceHelper.getStation(middleStationPos, robot));            Assert.AreEqual(new Position(5, 5), DistanceHelper.getStation(middleStationPos, robot));            Assert.AreNotEqual(fatherestStationPos, DistanceHelper.getStation(fatherestStationPos, robot));            Assert.AreEqual(new Position(4, 4), DistanceHelper.getStation(fatherestStationPos, robot));
        }

        [TestMethod]
        public void isEnoughEnergyToMoveTest()
        {
            Position posFrom = new Position(10, 10);            Position posTo = new Position(10, 15);            Assert.AreEqual(true, DistanceHelper.isEnoughEnergyToMove(posFrom, posTo, 25));
        }

        [TestMethod]
        public void getCloserToThePositionByNTest()
        {
            Position posFrom = new Position(10, 10);            Position posTo = new Position(10, 15);
            int n = DistanceHelper.getDistance(posFrom, posTo);

            Assert.AreEqual(posTo, DistanceHelper.getCloserToPositionByN(posFrom, posTo, n));
        }
    }
}