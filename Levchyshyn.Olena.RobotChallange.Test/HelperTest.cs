﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Robot.Common;

namespace Levchyshyn.Olena.RobotChallange.Test
{
    [TestClass]
    public class HelperTest
    {
        [TestMethod]
        public void getNearestStationTest()
        {
            Robot.Common.Robot robot = new Robot.Common.Robot()            {                Energy = 100,                Position = new Position(1, 1),                OwnerName = "Levchyshyn Olena"            };            Map map = new Map();            EnergyStation nearestStation = new EnergyStation()            {                Energy = 1000,                Position = new Position(10, 10),                RecoveryRate = 2            };            EnergyStation fatherestStation = new EnergyStation()            {                Energy = 1000,                Position = new Position(10, 15),                RecoveryRate = 2            };            map.Stations.Add(nearestStation);            map.Stations.Add(fatherestStation);            Assert.AreEqual(nearestStation, Helper.getNearestStation(robot, map.Stations));            Assert.AreNotEqual(fatherestStation, Helper.getNearestStation(robot, map.Stations));
        }

        [TestMethod]
        public void GetBetterStationTest()        {            Robot.Common.Robot robot = new Robot.Common.Robot()            {                Energy = 100,                Position = new Position(1, 1),                OwnerName = "Levchyshyn Olena"            };            Map map = new Map();            EnergyStation bestStation = new EnergyStation()            {                Energy = 1000,                Position = new Position(8, 5),
            };            EnergyStation worstStation = new EnergyStation()            {                Energy = 100,                Position = new Position(10, 15),
            };            map.Stations.Add(bestStation);            map.Stations.Add(worstStation);            Assert.AreEqual(bestStation, Helper.getBetterStation(robot, map.Stations));            Assert.AreNotEqual(worstStation, Helper.getBetterStation(robot, map.Stations));        }
    }
}