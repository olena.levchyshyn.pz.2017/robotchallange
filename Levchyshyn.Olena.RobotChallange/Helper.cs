﻿using Robot.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Levchyshyn.Olena.RobotChallange
{
    public class Helper
    {              public static EnergyStation getNearestStation(Robot.Common.Robot movingRobot, IList<EnergyStation> stations)        {            EnergyStation nearestStation = stations.First();            foreach (EnergyStation station in stations)            {                int distanceToStation = DistanceHelper.getDistance(movingRobot.Position, station.Position);                int distanceToNearestStation = DistanceHelper.getDistance(movingRobot.Position, nearestStation.Position);                if (distanceToStation < distanceToNearestStation)                {                    nearestStation = station;                }            }            return nearestStation;        }

        public static EnergyStation getBetterStation(Robot.Common.Robot movingRobot, IList<EnergyStation> stations)        {            
            EnergyStation maxEnergyStation = null;
            int maxEnergy = 0;
            foreach (EnergyStation station in stations)
            {
                if ((station.Energy > maxEnergy) && (movingRobot.Energy >
                    DistanceHelper.getDistance(movingRobot.Position, station.Position)))
                {
                    maxEnergyStation = station;
                    maxEnergy = station.Energy;
                }
            }
            return maxEnergyStation;
        }
    }    
}