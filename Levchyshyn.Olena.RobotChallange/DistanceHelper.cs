﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Robot.Common;

namespace Levchyshyn.Olena.RobotChallange
{
    public class DistanceHelper
    {
        public static int getDistance(Position from, Position to)        {            return (int)(Math.Pow(from.X - to.X, 2) + Math.Pow(from.Y - to.Y, 2));        }

        public static bool isEnoughEnergyToMove(Position a, Position b, int energy)        {            return getDistance(a, b) <= energy;        }        public static Position getCloserToPositionByN(Position a, Position b, int n)        {            if (getDistance(a, b) <= n) return b;            int newPosX = a.X, newPosY = a.Y;            if (a.X < b.X)            {                newPosX += n;            }            else if (a.X > b.X)            {                newPosX -= n;            }            if (a.Y < b.Y)            {                newPosY += n;            }            else if (a.Y > b.Y)            {                newPosY -= n;            }            return new Position(newPosX, newPosY);        }        public static Position getStation(Position station, Robot.Common.Robot robot)        {            if (isEnoughEnergyToMove(robot.Position, station, robot.Energy))            {                return station;            }            for (var n = getDistance(robot.Position, station); n >= 1; --n)            {                var currentPosition = robot.Position;                var energy = robot.Energy;                var ok = true;                while (currentPosition != station)                {                    var newPosition = getCloserToPositionByN(currentPosition, station, n);                    energy -= getDistance(currentPosition, newPosition);                    currentPosition = newPosition;                    if (energy <= 0)                    {                        ok = false;                        break;                    }                }                if (ok)                {                    return getCloserToPositionByN(robot.Position, station, n);                }            }            return null;        }
    }
}
