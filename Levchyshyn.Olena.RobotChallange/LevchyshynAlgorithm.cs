﻿using Robot.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Levchyshyn.Olena.RobotChallange
{
    public class LevchyshynAlgorithm : IRobotAlgorithm
    {
        public string Author => "Levchyshyn Olena";

        private short RoundCount { get; set; }
        public short RobotCount;
        private int CollectingDistance;

        public LevchyshynAlgorithm()
        {
            CollectingDistance = 2;
            RoundCount = 0;
            RobotCount = 10;
            Logger.OnLogRound += Logger_OnLogRound;
        }

        private void Logger_OnLogRound(object sender, LogRoundEventArgs e)
        {
            RoundCount++;
        }

        public RobotCommand DoStep(IList<Robot.Common.Robot> robots, int robotToMoveIndex, Map map)
        {
            Robot.Common.Robot robotToMove = robots[robotToMoveIndex];

            if (robotToMove.Energy > 400 && RoundCount < 45 && RobotCount < 100)
            {
                ++RobotCount;
                return new CreateNewRobotCommand();
            }

            EnergyStation station = Helper.getBetterStation(robotToMove, map.Stations) == null ?
             null : Helper.getNearestStation(robotToMove, map.Stations);
            Position newPos = station.Position;

            if (((DistanceHelper.getDistance(newPos, robotToMove.Position)) < CollectingDistance) && (station.Energy > 40))
            {
                return new CollectEnergyCommand();
            }
            else
            {
                newPos = DistanceHelper.getStation(newPos, robotToMove);
                return new MoveCommand() { NewPosition = newPos };
            }
        }
    }
}